
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        require: true
    },
    phone: {
        type: Number,
        required: true
    },
    dob: {
        type: Date,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    cpassword: {
        type: String,
        required: true
    },
    messages: [
        {
            name: {
                type: String,
                required: true
            },
            email: {
                type: String,
                require: true
            },
            phone: {
                type: Number,
                required: true
            },
            message: {
                type: String,
                required: true
            }
        }
    ],
    tokens:[
        {
        token:{
            type: String,
            required: true
        }
    }
    ]
});



// we are hashing the password

userSchema.pre('save', async function (next) {
    if (!this.isModified("password")) {
        return next();
    }
    // console.log("Hi I'm pre password");
    this.password = bcrypt.hashSync(this.password, 10);
    this.cpassword = bcrypt.hashSync(this.cpassword, 10);
    // console.log(this.password);
    // console.log(this.cpassword);
    next();
});

// generating token
userSchema.methods.generateAuthToken = async function(){
    try{
        let token = jwt.sign({_id:this._id}, process.env.SECRET_KEY);
        this.tokens = this.tokens.concat({token: token});
        await this.save();
        return token;
    }catch(err){
        console.log(err);
    }
}

//store the messages
userSchema.methods.addMessage = async function(name, email, phone, message) {
    try{
            this.messages = this.messages.concat({name, email, phone, message});
            await this.save();
            return this.messages;
    }catch(error) {
            console.log(error);
    }
}


// collection creation
const User = mongoose.model('USER', userSchema);
module.exports = User;
