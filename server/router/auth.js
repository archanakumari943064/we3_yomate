const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const authenticate = require("../middleware/authenticate");

const app = express();


require('../db/conn');
const User = require("../model/user");

router.get('/', (req, res) => {
     res.send('Hello World from server router js');
});


// Promises

// router.post('/register',  (req, res) => {

//     const { name, email, phone, dob, password, cpassword } = req.body;

//     if (!name || !email || !phone || !dob || !password || !cpassword)
//         return res.status(422).json({ error: "Fill up all the details properly" });

//     User.findOne({ email: email })
//         .then((userExist) => {
//             if (userExist)
//                 return res.status(422).json({ error: "Fill up all the container" });

//             const user = new User({ name, email, phone, dob, password, cpassword});

//             user.save().then(() => {
//                 res.status(201).json({message: "User registered succesfully"});
//             }).catch((err) => res.status(500).json({error: "Registration failed"}));

//         }).catch(err => { console.log(err); });

// });


// Async-await 

router.post('/signup', async (req, res) => {

   const { name, email, phone, dob, password, cpassword } = req.body;

   if (!name || !email || !phone || !dob || !password || !cpassword)
       return res.status(422).json({ error: "Fill up all the details properly." });

   try {
       const userExist = await User.findOne({ email: email });

       if (userExist)
           return res.status(422).json({ error: "Email already exists." });
           
       else if (password != cpassword)
           return res.status(422).json({ error: "Invalid credentials." });
       else {
           const user = new User({ name, email, phone, dob, password, cpassword });
           // hashing pass using 'pre'
           await user.save();

          // console.log(`${user} user Registered Successfully`);
          // console.log(userRegister);

           res.status(201).json({ message: "Registration succesfull." });
       }

   } catch (err) {
       console.log(err);
   }
});

// Login Route

router.post('/login', async (req, res) => {
  //  console.log(req.body);
   // res.json({message: "Awesome"});
 
   try {
        let token;
       const {email, password} = req.body;

      if (!email || !password) {
           return res.status(400).json({error: "Invalid credentials."})
       }

       const userLogin = await User.findOne({email: email});

    //   console.log(userLogin);
     //  res.json({message:"Sign-in Successfull."});

      if (userLogin) {
           const isMatch = await bcrypt.compare(password, userLogin.password);

      token = await userLogin.generateAuthToken();
      //console.log(token);

           res.cookie("jwtoken", token, {
               expires: new Date(Date.now() + 25892000000),
               httpOnly: true
           });

           if (!isMatch) {
               res.status(400).json({error: "Login Failed!"});
           }
           else {
               res.json({message: "Login Successfull."});
           }
       } else {
          res.status(400).json({error: "Login Failed!"});
       }

   } catch (err) {
       console.log(err);
   }
});

router.get('/about',authenticate, (req, res) => {
    console.log(`Hello my about`);
    res.send(req.rootUser);
});

//get userData fro contact us page & Home page.
router.get('/getdata',authenticate, (req, res) => {
    console.log(`Hello my about`);
    res.send(req.rootUser);
});

//about page:
//app.get('/about',authenticate,  (req, res) => {
  //  res.send('Hello World About');
  //  res.send('/about');
//});
router.post('/contactUs', authenticate, async (req, res) => {
    try{ 
       const {name, email, phone, message}  = req.body;
       if(!name || !email || !phone || !message){
           console.log("An error occurred in contact form.");
           return res.json({error: "Fill the form correctly."});
       }

       const userContact = await User.findOne({_id: req.userID});
       if(userContact) {
           const userMessage = await userContact.addMessage(name, email, phone, message);
           await userContact.save();
           res.status(201).json({message: "Message send successfully."});
       }

    } catch(error) {
        console.log(error);
    }
});

//about page:
app.get('/Signout', (req, res) => {
    console.log("Sign Out");
    res.clearCookie('jwtoken', {path: '/'});

    res.status(200).send('user Logout');
});

module.exports = router;