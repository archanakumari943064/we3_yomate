
const dotenv = require('dotenv');
const mongoose = require('mongoose');

const express = require('express');
const app = express();

dotenv.config({path: './config.env'});
require('./db/conn');

const User = require('./model/user');
//const User = require('./model/userSchema');

app.use(express.json());


//linking our router files to make our route easy
app.use(require('./router/auth'));

const PORT = process.env.PORT;

//middleware
//const middleware = (req, res, next) => {
//    console.log('This is middleware');
//    next();
//}

//app.get('/', (req, res) => {
//    res.send('Hello World');
//});

//app.get('/about', middleware, (req, res) => {
//  //  res.send('Hello World About');
//    res.send('/about');
//});

app.get('/login', (req, res) => {
    res.send('/login');
});


app.get('/signup', (req, res) => {
    res.send('/signup');
});
app.get('/profile', (req, res) => {
    res.send('/profile');
});
app.get('/help', (req, res) => {
    res.send('/help');
});
//app.get('/contactUs', (req, res) => {
    // res.cookie('jwtoken', 'dkjhf');
    //res.send('/contactUs');
//});
app.get('/supportUs', (req, res) => {
    res.send('/supportUs');
});
app.get('/signOut', (req, res) => {
    res.send('/signOut');
});

app.listen(PORT, () => {
    console.log(`server is running at port number: ${PORT}`);
});

