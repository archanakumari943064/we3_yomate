//type: 'rafce' app.js -> index.js(id: root) -> public:index.html
import React, {createContext, useReducer} from 'react'
import {Route, Switch} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css'
import './App.css';
import Navbar from "./components/Navbar";

import Home from "./components/Home";
import About from "./components/About";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Profile from "./components/AboutUs";
import Help from "./components/Help";
import ContactUs from "./components/ContactUs";
import SupportUs from "./components/SupportUs";
import Signout from "./components/Signout";
import ErrorPage from "./components/Errorpage";
import Webcam from "./components/Webcam";
import Search from "./components/Search";
import Info from "./components/Info";
import Youtubeid from "./components/Youtubeid";

import {initialState, reducer} from "../src/reducer/UseReducer";

export const UserContext = createContext();

const Routing = () => {
     return (
      <Switch>
      <Route exact path="/">
           <Home />
      </Route>
    
      <Route path="/about">
           <About />
      </Route>

      <Route path="/login">
           <Login />
      </Route>

      <Route path="/signup">
           <Signup />
      </Route>
      <Route path="/profile">
           <Profile />
      </Route>
      
      <Route path="/help">
           <Help />
      </Route>
  
      <Route path="/contactUs">
           <ContactUs />
      </Route>
      <Route path="/Webcam">
           <Webcam />
      </Route>
      <Route path="/Search">
           <Search />
      </Route>
      <Route path="/Youtubeid">
           <Youtubeid />
      </Route>
      <Route path="/Info">
           <Info />
      </Route>
  
      <Route path="/supportUs">
           <SupportUs />
      </Route>

      <Route path="/signOut">
           <Signout />
      </Route>

      <Route >
           <ErrorPage />
      </Route>    
      </Switch>
     )
}

const App = () => {
  
  const [state, dispatch] = useReducer(reducer, initialState)
  return (

    <>
    <UserContext.Provider value={{state, dispatch}}>
      <Navbar />
      <Routing />      
    </UserContext.Provider>
     
    </>
  )
}


export default App
