import React, {useEffect, useContext} from 'react'
import {useHistory} from "react-router-dom";
import {UserContext} from "../App";
import { NavLink } from 'react-router-dom';
const SignOut = () => {

   const {state, dispatch} = useContext(UserContext);

   //promises
   const history =  useHistory();
   useEffect(() => {
      fetch('/Signout', {
         method: "GET",
         headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
         },
         credentials: "include"
      }).then((res) => {
         dispatch({type:"USER", payload:false});
         history.push('/Signout', {replace: true});
         if(res.status != 200) {
            const error = new Error(res.error);
            throw error;
         }
      }).catch((err) => {
         console.log(err);
      })
   });

 return (
     <>
     <div className="bg3">
        <h1>Signout succesful</h1>  
        <br />
        <br />
        <NavLink className="nav-link active" aria-current="page" to="/Login"> <h1> Go back to Login page</h1></NavLink>
        </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
     </>
     )
}


export default SignOut
