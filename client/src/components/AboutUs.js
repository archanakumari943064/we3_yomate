import React from 'react'
import MyPic from '../images/MyPic.jpg';
import MyPic2 from '../images/MyPic2.png';

const AboutUs = () => {
    return (
        <>
            <div className = "bg7" >    
                <div className= "flex">           
                        <div className="flex-container">
                            <div className="profile-img">
                            <img src = {MyPic} alt = "My Pic" height = "300" /> 
                            </div>                                                   
                              <div className = "profile-head">
                                   <h3> <u><b>Archana Kumari</b></u></h3> 
                                   <h5> Student</h5>
                                   <h5>Sophomore@ University of Calcutta (IRPE)</h5>
                                   <p className="profile-rating mt-3 mb-5"></p>
                            </div>

                            <div className="profile-work">   
                            <a href="mailto:archanakumari943064@gmail.com" target="Gmail" className="btn btn-primary">Gmail </a>     
                            <a href="https://www.linkedin.com/in/archana-kumari-2198051bb/" target="linkedin" className="btn btn-primary">Linkedin</a>                        
                            <a href="https://github.com/archana-9430" target="Github" className="btn btn-primary"> Github</a>                        
                           
                            </div> 
                           
                        </div>

                        <div className="flex-container">
                            <div className="profile-img">
                            <img src = {MyPic2} alt = "My Pic" height = "300" /> 
                            </div>                               
                            <div className = "profile-head">
                                   <h3> <u><b>Rishika Raj</b></u></h3> 
                                   <h5> Student</h5>
                                   <h5>Sophomore@ Birla Institute of technology, Mesra</h5>
                                   <p className="profile-rating mt-3 mb-5"></p>
                            </div>
                            
                            <div className="profile-work">   
                            <a href="mailto:rishikaraj7263@gmailcom" target="Gmail" className="btn btn-primary">Gmail </a>     
                            <a href="https://www.linkedin.com/in/rishika-raj-79b970200/" target="linkedin" className="btn btn-primary">Linkedin</a>                        
                            <a href="https://github.com/Rishika-05" target="Github" className="btn btn-primary"> Github</a>                        
                           
                            </div> 
                        </div>
                               
                     </div>
               </div>
            
        </>
    )
}

export default AboutUs
