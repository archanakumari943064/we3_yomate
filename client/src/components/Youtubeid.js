import React, { Component, useState } from 'react';
import Select from 'react-select';
import 'bootstrap/dist/css/bootstrap.min.css';
import Info from './Info';
function Youtubeid() {

var Youtubeids = [
  { label: "Boat",  value: "QVEINjrYUPU" },
  { label: "Half-Boat", value: "0495rbXZMQg" },
  { label: "Bow", value: 'CZGtSaOvb50' },
  { label: "Bridge", value: "g78vfuC4QBI" },
  { label: "Butterfly", value: "kL-81iBucXo" },
  { label: "Camel", value: "_NNnowkcIqU" },
  { label: "Cat", value: "LGLIyrfTiUc" },
  { label: "Cow", value: 'W5KVx0ZbB_4' },
  { label: "Chair", value:  "tEZhXr0FuAQ" },
  { label: "Child's Pose", value: "2MJGg-dUKh0" },
  { label: "Corpse", value: "1VYlOKUdylM" },
  { label: "Crescent Lunge", value: "zaongQlYTto" },
  { label: "Crow", value: "ja2eab4sWlM" },
  { label: "Dolphin", value: "W9qEcTjmA78" },
  { label: "Downward-Facing Dog", value: "EC7RGJ975iM" },
  { label: "Eagle", value: "FTWFM-lL5jQ" },
  { label: "Extended Hand to Toe", value: "G8dFCt9V6yo" },
  { label: "Extended Side Angle", value: "aoB5L6AxckU" },
  { label: "Forearm Stand", value: "YFm6FVGtbMk" },
  { label: "Forward Bend with Shoulder Opener", value: "0kOOvLPN23Q" },
  { label: "Half-Moon", value: "csErxVR7dpA" },
  { label: "Handstand", value: "9u6wuuODdZo" },
  { label: "Low Lunge", value: "aOfniMZY2hk" },
{ label: "Pigeon", value: "WDOBkhKEuu0" },
  { label: "King Pigeon", value: "U2oimBogB4k" },
  { label: "Plank", value: "MGj1eknWWTo" },
  { label: "Plow", value: "guM2w_i55Vw" },
  { label: "Pyramid", value: "hA_m1TE9PMk" },
  { label: "Reverse Warrior", value: "NytDpa2r34g" },
  { label: "Seated Forward Bend", value: "T8sgVyF4Ux4" },
  { label: "Lotus", value: "w_j4lnfRC38" },
  { label: "Half Lord of the Fishes", value: "51EqCa6ZGCw" },
  { label: "Shoulder Stand", value: "UjHTOW9x3WM" },
  { label: "Side Plank", value: "_ByCVx-WxfQ" }, 
  { label: "Sphinx", value: "fOdrW7nf9gw" },
  { label: "Splits", value: "3ir0DFG5oMQ" },
  { label: "Squat", value: "fjzH1bd53lU" },
  { label: "Standing Forward Bend", value: "0kOOvLPN23Q" },
  { label: "Crescent Moon", value: "OVGohqTkiTk" },
  { label: "Side Splits", value: "YtjKXDHd5hU" },
  { label: "Tree", value: "qNSZxJpyhjM" },
  { label: "Triangle", value: "Fr5kiIygm0c" },
  { label: "Upward-Facing Dog", value: "pVmOOluGAv8" },
  { label: "Warrior One", value: "NytDpa2r34g" },
  { label: "Warrior Two", value: "Mn6RSIRCV3w" },
{ label: "Warrior Three", value: "uEc5hrgIYx4" },
{ label: "Wheel", value: "DPiW5pN1jQM" },
{ label: "Wild Thing", value: "CWS240CGV00" },
];
//class Youtubeid extends Component {
//  render() {
    const [result, idValue]  = useState(Youtubeids.value);
    const idHandler = e => {
        idValue(e.value);
    }
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-3"></div>
          <div className="col-md-6">
            <Select  options={Youtubeids} onChange={idHandler}/>
            <Info parentToChild={result}/>
          </div>
          <div className="col-md-4"></div>
        </div>
      </div>
    );
 // }
}

export default Youtubeid;
