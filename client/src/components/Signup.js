import React, {useState} from 'react'
import signuppic from '../images/logo1.png'
import { NavLink, useHistory } from 'react-router-dom';

const Signup = () => {
   const history= useHistory();
   const [user, setUser] = useState({
      name: "", email:"", phone:"", dob:"", password:"", cpassword:""
   });

   let name, value;
   const handleInputs = (e) => {
      console.log(e);
      name = e.target.name;
      value = e.target.value;

      setUser({...user, [name]:value});
   }

   const PostData = async (e) => {
      e.preventDefault();
      
      const {name, email, phone, dob, password, cpassword} = user;

      const res = await fetch("/signup", {
         method: "POST",
         headers: {
            "Content-Type": "application/json"
         },
         body:JSON.stringify({
            name, email, phone, dob, password, cpassword
         })
      });
      const data = await res.json();
      if(data.status === 422 || !data){
         window.alert("Inavlid credentials.");
         console.log("Invalid credentials");
      }else{
         window.alert("registration succesfull.");
         console.log("Registration succesful");
         history.push("/login");
      }

   }

   return (
    <>
      <section className="signup">
       <div className="bg5">
              <div className="signup-content">
                 <div className="signup-form">
                    <div class="flex-container">
                    <br />
                     <h2 className="form-title">Sign up</h2>
                     <form method="POST" className="register-form" id="register-form">
                        <div className="form-group">
                           <label htmlFor="name">
                              <i class="zmdi zmdi-account material-icons-name"></i>
                           </label>
                           <input type="text" name="name" id="name" autoComplete="off"
                           value={user.name}
                           onChange={handleInputs} 
                           placeholder="Your name " required></input>
                        </div>

                        <div className="form-group">
                           <label htmlFor="email" >
                              <i class="zmdi zmdi-email material-icons-name"></i>
                           </label>
                           <input type="email" name="email" id="email" autoComplete="off"
                           value={user.email}
                           onChange={handleInputs} 
                           placeholder="Your email"></input>
                        </div>
                        <div className="form-group">
                           <label htmlFor="phone">
                              <i class="zmdi zmdi-phone-in-talk material-icons-name"></i>
                           </label>
                           <input type="number" name="phone" id="phone" autoComplete="off"
                           value={user.phone}
                           onChange={handleInputs} 
                           placeholder="Your mobile number "></input>
                        </div>

                        <div className="form-group">
                           <label htmlFor="dob">
                           <i class="zmdi zmdi-calendar"></i>
                           </label>
                           <input type="date" name="dob" id="dob" autoComplete="off"
                           value={user.dob}
                           onChange={handleInputs} 
                           placeholder="D.O.B."></input>
                        </div>

                        <div className="form-group">
                           <label htmlFor="password">
                              <i class="zmdi zmdi-lock material-icons-name"></i>
                           </label>
                           <input type="password" name="password" id="password" autoComplete="off"
                           value={user.password}
                           onChange={handleInputs} 
                           placeholder="Password"></input>
                        </div>

                        <div className="form-group">
                           <label htmlFor="cpassword">
                              <i class="zmdi zmdi-lock material-icons-name"></i>
                           </label>
                           <input type="cpassword" name="cpassword" id="cpassword" autoComplete="off"
                           value={user.cpassword}
                           onChange={handleInputs} 
                           placeholder="Confirm password"></input>
                        </div>

                        <div className="form-group form-button">
                           <input type="submit" name="signup" id="signup" className="form-submit" value="register" onClick={PostData}></input>     
                        </div>
                     </form>
                     </div>      
                     </div>

                     <div class="flex-container">
                     <div className="signup-image">
                        <img src={signuppic} alt=" signup" />
                              <p >User Already registered? </p>
                        <NavLink to="/login" className="signup-image-link"> Login here</NavLink>
                        <br />
                        <br />
                 </div>
                 </div>

              </div>
         </div>
      </section>

    </>
   )
}

export default Signup
