import React, {useEffect, useState} from 'react'

const Contact = () => {
    const [userData, setUserData] = useState({name:'', email: '', phone:'', message: ""});

    const userContact = async() => {

      try{
             const res = await fetch('/getdata', {
                 method: 'GET',
                 headers: {
                    "Content-Type": "application/json"
                 },
             });

             const data = await res.json();
             console.log(data);
             setUserData({...userData, name: userData.name, email: userData.email, phone: userData.phone });

             if(!res.status == 200) {
                 const error  = new Error(res.error);
                 throw error;
             }

        } catch(err) {
             console.log(err);
        }
    }
    useEffect(() => {
        userContact();
    }, []);

//storing data in state
const handleInputs = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setUserData({...userData, [name]: value });
}

//send the data to backend
const contactForm = async (e) => {
    e.preventDefault();
    const {name, email, phone, message} = userData;
    const res = await fetch('/contactUs', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            name, email, phone, message
        })
    });
    const data = res.json();
    if(!data) {
        console.log("message not sent. An error occurred.");
    }else{
        alert("message sent successfully");
        setUserData({...userData, message: ""});
    }
}

 return (
      <>
          <div className="bg8">
                <div className="">
                    <div className="">
                        <div className="col-lg-10 offset-lg-1 d-flex justify-content-between">
                            <div className="contact_info_item d-flex justify-content-start align-items-center">
                            <i class="zmdi zmdi-phone"></i>
                                <div className="contact_info_content">
                                    <div className="contact_info_title">
                                        Phone
                                    </div>
                                    <div className="contact_info_text">
                                        +91 1111 222 3456
                                    </div>
                                </div>
                            </div>

                            <div className="contact_info_item d-flex justify-content-start align-items-center">
                            <i class="zmdi zmdi-email"></i>
                                <div className="contact_info_content">
                                    <div className="contact_info_title">
                                        Email
                                    </div>
                                    <div className="contact_info_text">
                                        xyz@gmail
                                    </div>
                                </div>
                            </div>

                            <div className="contact_info_item d-flex justify-content-start align-items-center">
                            <i class="zmdi zmdi-male-female"></i>
                                <div className="contact_info_content">
                                    <div className="contact_info_title">
                                        Address
                                    </div>
                                    <div className="contact_info_text">
                                        Bokaro, Jharkhand, India
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            

            <div className="contact_form">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-10 offset-lg-1">
                            <div className="contact_form_container py-5">
                                <div className="contact_form_title">
                                   <h4> <b>Get in Touch</b></h4> 
                                </div>
                                <form method="POST" id="contact_form">
                                    <div className="contact_form_name d-flex justify-content-between align-nav-item">
                                        <input type="text" name="contact_form_name" id="contact_form_name" onChange={handleInputs} name="name" value={userData.name} placeholder="Your Name" className="contact_form_name input_field" required />
                                        <input type="email" name="contact_form_email" id="contact_form_email" onChange={handleInputs} name="email" value={userData.email} placeholder="Your Email" className="contact_form_email input_field" required />
                                        <input type="phone" name="contact_form_phone" id="contact_form_phone" onChange={handleInputs} name="phone" value={userData.phone} placeholder="Your Phone No." className="contact_form_phone input_field" required />
                                    </div>
                                    <div className="contact_form_text mt-4 ">
                                        <textarea name="textfield contact_form_message" onChange={handleInputs} name="message" value={userData.message}  placeholder="Youe Message" cols="140" rows="5"></textarea>
                                    </div>
                                    <div className="contact_form_button">
                                        <button type="submit" className="button contact_submit_button" onClick={contactForm}>Send Message</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
            </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
      </>
     )
}

export default Contact
