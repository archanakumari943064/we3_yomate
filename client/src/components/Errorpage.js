import React from 'react'
import { NavLink } from 'react-router-dom';
 

const Errorpage = () => {
    return (
        <>
           <div id="notfound">
               <div className="notfound">
                   <div className="notfound-404">
                        <h1>404</h1>    
                   </div>
                   <div>
                       <h2>WE ARE SORRY. PAGE NOT FOUND</h2>
                   </div>
                    <p className="mb-5">
                        THE PAGE YOU ARE LOPKING FOR HAS BEEN REMOVED, HAD IT'S NAME CHANGED OR IS TEMPORARILY UNAVAILABLE.
                    </p>

                    <NavLink to='/'> <h5>Back to Homepage</h5></NavLink>
               </div>
           </div> 
        </>
    )
}

export default Errorpage
