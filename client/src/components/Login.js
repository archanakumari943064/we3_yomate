import React, {useState, useContext} from 'react'
import loginpic from "../images/logo2.png"
import { NavLink, useHistory } from 'react-router-dom';
import {UserContext} from "../App";

const Login = () => {

   const {state, dispatch} = useContext(UserContext);

   const history = useHistory();

   const [email, setEmail] = useState('');
   const [password, setPassword] = useState('');
   const loginUser = async (e) => {
      e.preventDefault();

      //ADDED THE BACKEND LINK
      const res = await fetch('https://git.heroku.com/yomate.git/login', {
         method:"POST",
         headers:{
            "Content-Type": "application/json"
         },
          body: JSON.stringify({
            email,
            password
         })
      });
      const data = res.json();
      if(res.status === 400 || !data){
         window.alert("Invalid credentials.");
      }else{
         dispatch({type:"USER", payload:true});
         window.alert("Login Successful.");
         history.push("/about");
      }
   }

    return (
   <>
      <section className="signin">
         <div className="bg5">
              <div className="signin-content">

                 <div className="signin-image">
                        <img src={loginpic} alt=" login" />
                              <p>Not registered yet? </p>
                        <NavLink to="/signup" className="signin-image-link"> Create an Account</NavLink>
                 </div>

                 <div className="signin-form">
                     <h2 className="form-title">Login</h2>
                     <form method="POST" className="register-form" id="register-form">
                       
                        <div className="form-group">
                           <label htmlFor="email" >
                              <i class="zmdi zmdi-email material-icons-name"></i>
                           </label>
                           <input type="email" name="email" id="email" autoComplete="off" 
                           value={email}
                           onChange={(e) => setEmail(e.target.value)}
                           placeholder="Your email"></input>
                        </div>
                        
                        <div className="form-group">
                           <label htmlFor="password">
                              <i class="zmdi zmdi-lock material-icons-name"></i>
                           </label>
                           <input type="password" name="password" id="password" autoComplete="off" 
                             value={password}
                             onChange={(e) => setPassword(e.target.value)}
                           placeholder="Password"></input>
                        </div>

                        <div className="form-group form-button">
                           <input type="submit" name="signin" id="signin" className="form-submit" value="Login"
                           onClick={loginUser}
                           />
                        </div>
                     </form>
                     </div>      
                     
              </div>
         </div>
      </section>

   </>
 )
}

export default Login
