import React, {useEffect} from 'react'
import i0 from '../images/yoga3.jpg';
import i1 from '../images/logo13.jpg';
import i2 from '../images/yoga2.jpg';
const About = () => {

  const callAboutPage = async () => {
    try{
      const res = await fetch('/about')
    }
    catch(err) {
      console.log(err);
    }
  }

  useEffect(() => {
    callAboutPage();
  }, []);
 return (
  <>
   <div className="">
                <div className="about">
                    <h5>WELCOME to YoMate!</h5>
                    <h1><b>Services We Provide</b></h1>
                    <br />
                <div className="">
                    <div className="container">
  <div className="row my-0">
<div className="col">
<div className="card .bg-dark" >
  <img src={i0} className="card-img-top" alt="..." height="200px" />
  <div className="card-body .bg-dark">
    <h4 className="card-title"> List of Yoga Asanas </h4>
    <p className="card-text">  Unlimited access to a exclusive list yoga and meditation asanas. </p>
    <a href="/Search" className="btn btn-primary">See List</a>
  </div>
</div>
</div>
<div className="col">
    <div className="card" >
  <img src={i1} className="card-img-top" alt="..." height="200px" />
  <div className="card-body">
    <h4 className="card-title">Yoga Videos</h4>
    <p className="card-text">Easy access to detailed explanatory videos.</p>
    <a href="/Youtubeid" className="btn btn-primary">Watch Video</a>
  </div>
</div>
</div>
    <div className="col">
      <div className="card" >
  <img src={i2} className="card-img-top" alt="..." height="200px" />
  <div className="card-body">
    <h4 className="card-title">Live webcam</h4>
    <p className="card-text"> Check your performance live with webcam feature.</p>
    <a href="/webcam" className="btn btn-primary">Go Live</a>
  </div>
</div>
    </div>
  </div>
</div>
</div>

<div className="about">
    <h1>About Yoga and meditation:</h1>

      <h6 text-align="center">
         Yoga and Meditation brings together physical and mental disciplines to achieve a peaceful body and mind.
        It helps to manage stress and anxiety and keeps us relaxing. 
        It improves respiration, energy and vitality.
        Yoga asanas build strength, flexibility and confidence.
        It helps to boost our brain power and concentration.
        It plays a significant role in the psycho-social care, managing blood pressure and heart rate.
        The art of practising Yoga and Meditation helps in controlling an individual's mind, body and soul.
      </h6>
      <h5>But Practising Yoga & Meditation Incorrectly Can Harm your body.</h5>
    
      <h3>Thus, YoMate brings to you the one stop solution for Yoga and Meditation.</h3>
      <h3><u>Unlock the real you with YoMate.</u></h3>
  </div>                  

{/* <div className="about">
<h1>What are the services we provide:</h1>
<h4>
 <ul>
  <li><div>Free access to quick yoga poses.</div></li>
  <li><div>Unlimited access to free explanatory videos.</div></li>
  <li><div>Check your scores on a daily basis.</div></li>
  <li><div>Practice more earn more points.</div></li>
</ul>
</h4>
<br /> 
<h3><u>Unlock the real you with YoMate.</u></h3>
</div> */}


    </div>    
    </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
  </>
 )
}

export default About
