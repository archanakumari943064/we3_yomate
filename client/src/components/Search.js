import React, {useState, useEffect,Component} from 'react';
import YouTube from "react-youtube";
import { NavLink } from 'react-router-dom';
import Youtubeid from "./Youtubeid";
var getYouTubeID = require("get-youtube-id");


// var youtube_Key = "AIzaSyBcY7xBC8qSNeUQv39-csOZ6sWqitZ1BJw";

const Search =() => {

  const [users, setUsers] = useState([]);

// ACCESS YOGA JSON API
  const listOfAllYogaPoses = async () => {
    //const response = await fetch('https://raw.githubusercontent.com/rebeccaestes/yoga_api/master/yoga_api.json')
    //const response = await fetch('https://gitlab.com/Rishika-05/yoga_vapi/-/raw/master/yoga_vapi.json')
    const response = await fetch('https://raw.githubusercontent.com/archana-9430/yoga_API/main/api.json')
    setUsers(await response.json());
  }

  {/*const youtube_id = () => {
    console.log("clicked");
    <NavLink to="/Info" className="signin-image-link"> See</NavLink>
  }
*/}

  useEffect(() => {
    listOfAllYogaPoses();
  }, []);

    return (
        <>
        <div className="bgS">
            <h1 text-align="center"> <u>List of Yoga Poses.</u></h1>
            <NavLink to="/Youtubeid" className="signin-image-link" text-align="right"><h4> Watch Video </h4></NavLink>
            <div className="container-fluid mt-5">
              <div className="row text-center">
            {
            users.map((Data) => {
              return(             
                <div className="col-10 col-md-4 mt-5">
                  <div className="card p-2">
                    <div className="d-flex align-items-center">
                      <div className="image"><img src={Data.img_url} className="rounded" width="200"/></div>                 
                      <div className="ml-3 w-100">
                        <h4 className="mb-0 mt-0 textLeft">{Data.id} </h4>
                          <div className="p-2 mt-2 bg-info d-flex justify-content-between rounded text-white stats">
                            <div className="d-flex flex-column"> 
                            <span className="articles"><p><i>English Name:</i> <u>{Data.english_name}</u></p></span>
                            <span className="articles"><p><i>Sanskrit Name:</i> <u> {Data.sanskrit_name}</u></p></span>
                          {/*  <span className="articles"><h6>Decsription: {Data.description}</h6></span> */}
                            <div className="form-group form-button">
                            {/*<button type="submit" className="form-submit" value="See video"
                             onClick={youtube_id} height="30px"/> */}
                         
                            </div>
                           </div>
                          </div>                   
                        </div>                   
                      </div>
                    </div>
                  </div>
                )
              })
            }                
          </div>
        </div>
       </div> 
     </>
    )
}

export default Search
