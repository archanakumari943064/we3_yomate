import React, {useContext} from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from '../images/logo3.png';
import { NavLink } from 'react-router-dom';
import { UserContext } from '../App';



const Navbar = () => {
  const {state, dispatch} = useContext(UserContext);
const RenderMenu = () => {
  if(state){
    return (
      <>
        <li className="nav-item">
          <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/about">About</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/Webcam">Webcam</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/Search">See Poses</NavLink>
        </li>
       
        <li className="nav-item dropdown">
        <NavLink className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
         aria-expanded="false">
            More
          </NavLink>
          <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
          <li><NavLink className="dropdown-item" to="/profile">About Us</NavLink></li>
            <li><NavLink className="dropdown-item" to="/help">Help</NavLink></li>
            <li><NavLink className="dropdown-item" to="/contactUs">Contact Us</NavLink></li>
            <li><NavLink className="dropdown-item" to="/supportUs">Support Us</NavLink></li>
            <li><hr className="dropdown-divider" /></li>
            
          </ul>
        </li>

       

        <li className="nav-item">
          <NavLink className="nav-link" to="/signOut">Sign-Out</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to=""></NavLink>
        </li>
      </>
    )
  } else {
    return (
      <>
        <li className="nav-item">        
          <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
        </li>
       {/*} <li className="nav-item">
          <NavLink className="nav-link" to="/about">About</NavLink>
        </li>*/}
        <li className="nav-item">
          <NavLink className="nav-link" to="/login">Login</NavLink>
        </li>
        
       
        {/* <li className="nav-item">
          <NavLink className="nav-link" to="/Webcam">Webcam</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/Search">See Poses</NavLink>
        </li> */}
        <li className="nav-item dropdown">
        <NavLink className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
         aria-expanded="false">
            More
          </NavLink>
          <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
          <li><NavLink className="dropdown-item" to="/profile">About Us</NavLink></li>
            <li><NavLink className="dropdown-item" to="/help">Help</NavLink></li>
            <li><NavLink className="dropdown-item" to="/contactUs">Contact Us</NavLink></li>
            <li><NavLink className="dropdown-item" to="/supportUs">Support Us</NavLink></li>
          </ul>
        </li>

        <li className="nav-item">
          <NavLink className="nav-link" to="/signup">Sign-up</NavLink>
        </li>

        <li className="nav-item">
          <NavLink className="nav-link" to=""></NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to=""></NavLink>
        </li>
      </>
    )
  }
}

 return (
   <>
  <nav className="navbar navbar-expand-lg navbar-dark bg-primary" >
  <div className="container-fluid">
    <NavLink className="navbar-brand" to="#">
      <img src={logo} alt="logo" width="30" height="24" className="d-inline-block align-top" />
      YoMate</NavLink>
      <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" 
    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav ms-auto text-center">
          <RenderMenu />

      </ul>
     {// <form class="d-flex">
 //
      //  <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
      //  <button class="btn btn-outline-success" type="submit">Search</button>

 
    //  </form>
      }
    </div>
  </div>
</nav>  


   </>
 )
}


export default Navbar
